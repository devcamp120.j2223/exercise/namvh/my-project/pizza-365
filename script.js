$(document).ready(function(){
    onPageLoading();
});
//REGION 1:
//khai báo dữ liệu biến toàn cầu
var gSelectedMenuStructure = {
    menuName: "", //S,M,L
    duongKinhCM: 0,
    suonNuong: 0,
    saLad: 0,
    drink: 0,
    priceVND: 0
}
var gSELECT_NULL_VALUE = "NONE";
var gSelectedPizzaType = "";

// Định nghĩa đối tượng chứa thông tin đơn hàng
  var gOrder = {
    fullName: "",
    email: "",
    phoneNumber: "",
    address: "",
    voucherId: "",
    message: "",
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: "",
    thanhTien: "",
    
  };

// Định nghĩa biến lưu thông tin phần trăm giảm giá, mặc định bằng 0
var gVOUCHER_PERCENT_DISCOUNT;
//REGION 2:
function onPageLoading(){
    "use strict"
    //call API để lấy danh sách nước
    $.ajax({
     url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
     data: "GET",
     dataType: "json",
     success: function(res){
        //  console.log(res);
         loadDrinkListToSeclet(res);
     },
     error: function(ajaxContext){
         alert(ajaxContext.responseText);
     }  
    });
}
 //-[1]----function click đổi màu button và hiển thị ra console nội dung--------//   
 $("#btn-small").on("click", function(){
    onBtnSmallPizzaType();
 });

 $("#btn-medium").on("click", function(){
    onBtnMediumPizzaType();
 });

 $("#btn-large").on("click", function(){
    onBtnLargePizzaType();
 });
 //-----function done range--------// 

 //-[2]----function click đổi màu button loại pizza--------// 
$("#hawai-pizza").on("click", function(){
    onBtnHawaiPizzaClick();
});

$("#sea-food-pizza").on("click", function(){
    onBtnSeaFoodPizzaClick();
});

$("#bacon-pizza").on("click", function(){
    onBtnBaconPizzaClick();
});
 //-[3]----function click send hiển thị thông tin ra console--------// 
 $("#btn-send").on("click", function(){
    onBtnSendClick();
 });
 //-----function done range--------// 
$("#btn-tao-don").on("click", function(){
    onBtnCreateOrderClick();
});
 //-[4]----function click tạo đơn hiển thị ra 1 modal khác--------// 

//-----function done range--------// 
//REGION 3:
 //-----function thực hiện tác vụ vs button--------//   
 //Small Button
 function onBtnSmallPizzaType(){
    "use strict";
//map lựa chọn vào gSelectedMenuStructure
    gSelectedMenuStructure.menuName = "S";
    gSelectedMenuStructure.duongKinhCM = "20 CM";
    gSelectedMenuStructure.suonNuong = 2;
    gSelectedMenuStructure.saLad = "200G";
    gSelectedMenuStructure.drink = 2;
    gSelectedMenuStructure.priceVND = 150000
//gắn màu sắc vào 
    setPizzaTypeColorButton(gSelectedMenuStructure.menuName);
//hiển thị nội dung ra console
    console.log("%c Menu chọn: " + gSelectedMenuStructure.menuName ,"color:red");
    console.log("%c Đường kính Pizza: " + gSelectedMenuStructure.duongKinhCM ,"color:red");
    console.log("%c Sườn nướng: " + gSelectedMenuStructure.suonNuong ,"color:red");
    console.log("%c Salad: " + gSelectedMenuStructure.saLad ,"color:red");
    console.log("%c Drink: " + gSelectedMenuStructure.drink ,"color:red");
    console.log("%c priceVND: " + gSelectedMenuStructure.priceVND ,"color:red");
}

//Medium Button
function onBtnMediumPizzaType(){
    "use strict";
//map lựa chọn vào gSelectedMenuStructure
    gSelectedMenuStructure.menuName = "M";
    gSelectedMenuStructure.duongKinhCM = "25 CM";
    gSelectedMenuStructure.suonNuong = 4;
    gSelectedMenuStructure.saLad = "300G";
    gSelectedMenuStructure.drink = 3;
    gSelectedMenuStructure.priceVND = 200000
//gắn màu sắc vào 
    setPizzaTypeColorButton(gSelectedMenuStructure.menuName);
//hiển thị nội dung ra console
    console.log("%c Menu chọn: " + gSelectedMenuStructure.menuName ,"color:green");
    console.log("%c Đường kính Pizza: " + gSelectedMenuStructure.duongKinhCM ,"color:green");
    console.log("%c Sườn nướng: " + gSelectedMenuStructure.suonNuong ,"color:green");
    console.log("%c Salad: " + gSelectedMenuStructure.saLad ,"color:green");
    console.log("%c Drink: " + gSelectedMenuStructure.drink ,"color:green");
    console.log("%c priceVND: " + gSelectedMenuStructure.priceVND ,"color:green");
}

//Large Button
function onBtnLargePizzaType(){
    "use strict";
//map lựa chọn vào gSelectedMenuStructure
    gSelectedMenuStructure.menuName = "L";
    gSelectedMenuStructure.duongKinhCM = "30 CM";
    gSelectedMenuStructure.suonNuong = 8;
    gSelectedMenuStructure.saLad = "500G";
    gSelectedMenuStructure.drink = 4;
    gSelectedMenuStructure.priceVND = 250000
//gắn màu sắc vào 
    setPizzaTypeColorButton(gSelectedMenuStructure.menuName);
//hiển thị nội dung ra console
    console.log("%c Menu chọn: " + gSelectedMenuStructure.menuName ,"color:blue");
    console.log("%c Đường kính Pizza: " + gSelectedMenuStructure.duongKinhCM ,"color:blue");
    console.log("%c Sườn nướng: " + gSelectedMenuStructure.suonNuong ,"color:blue");
    console.log("%c Salad: " + gSelectedMenuStructure.saLad ,"color:blue");
    console.log("%c Drink: " + gSelectedMenuStructure.drink ,"color:blue");
    console.log("%c priceVND: " + gSelectedMenuStructure.priceVND ,"color:blue");
}
 //-----function done range--------// 

 //=========function thực hiệc đổi pizzatype button color=============//
 function onBtnHawaiPizzaClick(){
     "use strict";
//map lựa chọn vào gSelectedPizzaType
     gSelectedPizzaType = "OCEAN MANIA";
//gắn màu sắc vào
    setPizzaNameColorButton(gSelectedPizzaType);
//hiển thị console.log
    console.log("%c Loại Pizza được khách hàng chọn: " + gSelectedPizzaType ,"color:purple");    

 }

 function onBtnSeaFoodPizzaClick(){
    "use strict";
//map lựa chọn vào gSelectedPizzaType
    gSelectedPizzaType = "HAWAIIAN";
//gắn màu sắc vào
   setPizzaNameColorButton(gSelectedPizzaType);
//hiển thị console.log
   console.log("%c Loại Pizza được khách hàng chọn: " + gSelectedPizzaType ,"color:purple");    

}

function onBtnBaconPizzaClick(){
    "use strict";
//map lựa chọn vào gSelectedPizzaType
    gSelectedPizzaType = "CHEESY CHICKEN BACON";
//gắn màu sắc vào
   setPizzaNameColorButton(gSelectedPizzaType);
//hiển thị console.log
   console.log("%c Loại Pizza được khách hàng chọn: " + gSelectedPizzaType ,"color:purple");    

}
 //-----function done range--------// 

 //function thực hiện hiển thị nội dung gửi đơn hàng ra console
 function onBtnSendClick(){
    "use strict";     
  //B1: thu thập dữ liệu
  getDataFormOrder(gOrder);
  //B2: validate
  var vIsCheck = validateData(gOrder);  
  if(vIsCheck){

    if( gOrder.voucherId == ""){
        gVOUCHER_PERCENT_DISCOUNT = 0;
        disPlayDataIntoModal(gOrder);
    }
    else if (gOrder.voucherId != ""){
        checkVoucherId(gOrder.voucherId, gOrder);

    };
  }
 } ;

//function thực hiện click để tạo đơn
function onBtnCreateOrderClick(){
    "use strict";
 //B0: khai báo biến
 var vObjOrder = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: "",
    hoTen: "",
    thanhTien: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: ""
 };   
 //B1: thu thập dữ liệu
 getDataCreateOrder(vObjOrder);
 //B2:validate dữ liệu ( không cần)
 //B3: call API để POST dữ  liệu lên server
 callApiToPostOrder(vObjOrder);
 //B4: thực hiện display front-end
 //Ẩn Modal đơn hàng
 $("#order-info-modal").modal("hide");
 //Hiển model xác nhận create order bằng OrderId
 $("#create-orderid-modal").modal("show");

}
//REGION 4:
//function thực hiện đổi màu button Pizza Type
function setPizzaTypeColorButton(paramSelectedMenuStructure){
    "use strict";
    // truy xuất và gắn giá trị màu cho button
    console.log("hiển thị Pizza type được chọn: " + paramSelectedMenuStructure);
    switch(paramSelectedMenuStructure){
      case "S" :
          $("#btn-small").css({"background-color":"#ffc107","border-color":"#ffc107"});//màu cam
          $("#btn-medium").css({"background-color":"#28a745","border-color":"#28a745"});//màu xanh
          $("#btn-large").css({"background-color":"#28a745","border-color":"#28a745"});//màu xanh 
      break;
      case "M" :
          $("#btn-small").css({"background-color":"#28a745","border-color":"#28a745"});//màu xanh
          $("#btn-medium").css({"background-color":"#ffc107","border-color":"#ffc107"});//màu cam
          $("#btn-large").css({"background-color":"#28a745","border-color":"#28a745"});//màu xanh
      break;
      case "L" :    
          $("#btn-small").css({"background-color":"#28a745","border-color":"#28a745"});//màu xanh
          $("#btn-medium").css({"background-color":"#28a745","border-color":"#28a745"});//màu xanh
          $("#btn-large").css({"background-color":"#ffc107","border-color":"#ffc107"});//màu cam
    }
  }

function setPizzaNameColorButton(paramSelectedPizzaType){
    "usetrict";
    if(paramSelectedPizzaType == "OCEAN MANIA"){
        $("#hawai-pizza").css({"background-color":"#ffc107","border-color":"#ffc107"});//màu cam
        $("#sea-food-pizza").css({"background-color":"#28a745","border-color":"#28a745"});//màu xanh
        $("#bacon-pizza").css({"background-color":"#28a745","border-color":"#28a745"});//màu xanh     
    }
    else if( paramSelectedPizzaType == "HAWAIIAN"){
        $("#hawai-pizza").css({"background-color":"#28a745","border-color":"#28a745"});//màu xanh
        $("#sea-food-pizza").css({"background-color":"#ffc107","border-color":"#ffc107"});//màu cam
        $("#bacon-pizza").css({"background-color":"#28a745","border-color":"#28a745"});//màu xanh
    }
    else if( paramSelectedPizzaType == "CHEESY CHICKEN BACON"){
        $("#hawai-pizza").css({"background-color":"#28a745","border-color":"#28a745"});//màu xanh
        $("#sea-food-pizza").css({"background-color":"#28a745","border-color":"#28a745"});//màu cam
        $("#bacon-pizza").css({"background-color":"#ffc107","border-color":"#ffc107"});//màu xanh
    }
}

//function thực hiện list drinklist vào select
function loadDrinkListToSeclet(paramDinkList){
    "use strict";
//truy xuất phần tử select
    var vDrinkListSelect = $("#select-list-drink");
    // $("<option/>", { 
    //     text: "--- Chọn tất cả  ---",
    //     value: 0
    // }).appendTo(vDrinkListSelect);

    // Làm rỗng dữ liệu của select trước khi append
    // $("#select-list-drink").empty();
    for(var bI = 0; bI < paramDinkList.length; bI ++){
        $("<option/>", {
            text: paramDinkList[bI].maNuocUong,
            // value: paramDinkList[bI]
        }).appendTo(vDrinkListSelect);
    }
}

//function thựu hiện hiển thị dữ liệu ra console
// function showDataIntoConsole(){
//     "use strict";
//     console.log($("#inp-fullname").val().trim());
//     console.log($("#inp-email").val().trim());
//     console.log($("#inp-phonenumber").val().trim());
//     console.log($("#inp-address").val().trim());
//     console.log($("#inp-coupon").val().trim());
//     console.log($("#inp-message").val().trim());
// }

//function thực hiện hiển thị dữ liệu 
function getDataFormOrder(paramOrder){
    "use stict";
    paramOrder.fullName = $("#inp-fullname").val().trim();
    paramOrder.email = $("#inp-email").val().trim();
    paramOrder.phoneNumber = $("#inp-phonenumber").val().trim();
    paramOrder.address = $("#inp-address").val().trim();
    paramOrder.voucherId = $("#inp-coupon").val().trim();
    paramOrder.message = $("#inp-message").val().trim();

    //Gan gia tri cho Order Object
    paramOrder.duongKinh = gSelectedMenuStructure.duongKinhCM;
    paramOrder.kichCo = gSelectedMenuStructure.menuName;
    paramOrder.suon = gSelectedMenuStructure.suonNuong;
    paramOrder.salad = gSelectedMenuStructure.saLad;
    paramOrder.loaiPizza = gSelectedPizzaType;
    paramOrder.idLoaiNuocUong = $("#select-list-drink").find(':selected').val();
    paramOrder.soLuongNuoc = gSelectedMenuStructure.drink;
}

//function validate dữ liệu
function validateData(paramOrder){
    "use strict";
    if(paramOrder.fullName == ""){
        alert("cần nhập tên!!!");
        return false;
    }
    if (paramOrder.email == ""){
        alert("cần nhập email");
        return false;
    }
    if(!validateEmail(paramOrder.email)){
        alert("định dạng email chưa chính xác");
        return false;
    }
    if(paramOrder.phoneNumber == "" || isNaN(paramOrder.phoneNumber) ) {
        alert("chưa điền phone number");
        return false;
    }
    if(paramOrder.address == ""){
        alert("cần nhập địa chỉ");
        return false;
    }

    if (paramOrder.kichCo === gSELECT_NULL_VALUE || paramOrder.kichCo==="") {
        alert("Bạn cần chọn một kiểu Pizza!");
        return false;
      }
      if (paramOrder.idLoaiNuocUong == 0) {
        alert("Bạn cần chọn một loại đồ uống!");
        return false;
      }
      if (paramOrder.loaiPizza === gSELECT_NULL_VALUE || paramOrder.loaiPizza==="") {
        alert("Bạn cần chọn một loại Pizza!");
        return false;
      }
      return true;
}
//function dùng để validate email
function validateEmail(paramEmail){
    "use strict";
    const vREG = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return vREG.test(String(paramEmail).toLocaleLowerCase());
    }

//function thực hiện check idVoucher có hay không ( call API)
function checkVoucherId(paramVoucherId, paramOrder ) {
    "use-strict";
    // Gọi Api lấy kết quả voucherId
    var gBASE_URL_VOUCHER_ID = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";
    $.ajax({
        url: gBASE_URL_VOUCHER_ID + paramVoucherId,
        type: 'GET',
        dataType: 'json',
        success: function (Res) {
            gVOUCHER_PERCENT_DISCOUNT = Res.phanTramGiamGia;
        //    console.log("hiển thị Id; " + gVOUCHER_PERCENT_DISCOUNT);
            // Thông báo người dùng mã hợp lệ
            alert('Chúc mừng bạn. Voucher này là hợp lệ');
            disPlayDataIntoModal(paramOrder);
        },
        error: function (ajaxContext) {
            // Thông báo lỗi nội dung khi error
            gVOUCHER_PERCENT_DISCOUNT = 0;
            disPlayDataIntoModal(paramOrder)
            alert("Không tìm thấy Voucher: " + ajaxContext.responseText);
            return true;
        }
        
    })
}

//function thực hiện hiển thị dữ liệu ra modal
// Hàm ghi thông tin order ra console
function disPlayDataIntoModal(paramOrder) {
    "use-strict";
// dispaly ra console
    console.log('Họ và tên khách hàng: ' + paramOrder.fullName);
    console.log('Email của khách hàng: ' + paramOrder.email);
    console.log('Số điện thoại của khách hàng: ' + paramOrder.phoneNumber);
    console.log('Địa chỉ của khách hàng: ' + paramOrder.address);
    console.log('Mã giảm giá của khách hàng: ' + paramOrder.voucherId);
    console.log('Tin nhắn của khách hàng: ' + paramOrder.message);

    console.log("loai nuoc uong duoc chon: " + paramOrder.idLoaiNuocUong);
    // Load thông tin đơn hàng ra form Modal
    $('#input-fullname').val(paramOrder.fullName);
    $('#input-phonenumber').val(paramOrder.phoneNumber);
    $('#input-address').val(paramOrder.address);
    $('#input-message').val(paramOrder.message);
    $('#input-coupon').val(paramOrder.voucherId);
    
    // Định nghĩa biến lưu giá trị thành tiền phải thanh toán
    // var vTotalMoney = gSelectedMenuStructure.priceVND - ( (gVOUCHER_PERCENT_DISCOUNT/100) * gSelectedMenuStructure.priceVND );
    var vTotalMoney = gSelectedMenuStructure.priceVND - (gSelectedMenuStructure.priceVND * gVOUCHER_PERCENT_DISCOUNT) / 100;

    // Phần thông tin tổng hợp đơn hàng
    $('#inp-text-area').text( 'Xác nhận: ' +  paramOrder.fullName + 
                                    ' / ' + paramOrder.phoneNumber + 
                                    ' / ' + paramOrder.address + '\n' + 
                                    'Menu: ' + gSelectedMenuStructure.menuName + 
                                    ' / Sườn nướng  ' + gSelectedMenuStructure.suonNuong + 
                                    ' / Nước uống ' + gSelectedMenuStructure.drink  + 
                                    ' / Salad ' + gSelectedMenuStructure.saLad + '\n' + 
                                    'Loại pizza: ' + gSelectedPizzaType + '\n' +
                                    'Loại nuoc: ' + paramOrder.idLoaiNuocUong + 
                                    ' / Giá  ' + gSelectedMenuStructure.priceVND +
                                    ' /  Mã giảm giá  ' + gOrder.voucherId + '\n' + 
                                    'Phải thanh toán: ' + vTotalMoney + 'vnđ (giảm giá ' + gVOUCHER_PERCENT_DISCOUNT + '%)');
    // Hiển thị Modal
    $('#order-info-modal').modal('show');
}

//function khai báo biến để tạo order
function getDataCreateOrder(paramObjOrder){
    "use strict";
    paramObjOrder.kichCo = gSelectedMenuStructure.menuName;
    paramObjOrder.duongKinh = gSelectedMenuStructure.duongKinhCM;
    paramObjOrder.suon = gSelectedMenuStructure.suonNuong;
    paramObjOrder.salad = gSelectedMenuStructure.saLad;
    paramObjOrder.loaiPizza = gSelectedPizzaType;
    paramObjOrder.idVourcher = gOrder.voucherId;
    paramObjOrder.idLoaiNuocUong = $("#select-list-drink").find(':selected').val();
    paramObjOrder.soLuongNuoc = gSelectedMenuStructure.drink;
    paramObjOrder.hoTen = gOrder.fullName;
    paramObjOrder.thanhTien = gSelectedMenuStructure.priceVND;
    paramObjOrder.email =  gOrder.email;
    paramObjOrder.soDienThoai =gOrder.phoneNumber;
    paramObjOrder.diaChi = gOrder.address;
    paramObjOrder.loiNhan = gOrder.message;
} 

//function thực hiện call API to POST order
function callApiToPostOrder(paramObjOrder){
    "use strict";
    $.ajax({
        url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
        type: "POST",
        contentType: 'application/json',
        data: JSON.stringify(paramObjOrder),
        success: function(res){
            // console.log("%c display order create: " + res, "color:red");
            $("#order-id").val(res.orderId);
        },
        eror: function(ajaxContext){
            alert(ajaxContext.responseText);
        }
    });
}